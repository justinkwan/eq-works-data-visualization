# EQ-Works Data Visualizer
A simple data visualization web-app that displays API data from EQ-Works API, built with React, Next.JS and Chart.js

## Features
- Line, bar and donut charts, displaying several different data sets
- Data normalization for multi-field  qualitative trend analysis
